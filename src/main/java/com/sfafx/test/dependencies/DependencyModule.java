package com.sfafx.test.dependencies;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class DependencyModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(CommonData.class).in(Singleton.class);
    }
}
package com.sfafx.test.dependencies;

public class CommonData {
    private String commonString = "Common String";

    public String getCommonString() {
        return commonString;
    }

    public void setCommonString(String commonString) {
        this.commonString = commonString;
    }
}

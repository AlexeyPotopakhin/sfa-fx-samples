package com.sfafx.test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sfafx.SingleFormApplication;
import com.sfafx.test.dependencies.DependencyModule;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public class TestApplication extends SingleFormApplication {
    public static final String VIEW_ONE = "View One";
    public static final String VIEW_TWO = "View Two";

    @Override
    public void initialize() {
        Injector injector = Guice.createInjector(new DependencyModule());

        Parent viewOne = null;
        Parent viewTwo = null;
        try {
            // View 1 fxml loader
            FXMLLoader viewOneFxmlLoader = new FXMLLoader();
            viewOneFxmlLoader.setLocation(TestApplication.class.getResource("views/one/one.fxml"));
            viewOneFxmlLoader.setControllerFactory(injector::getInstance);
            viewOne = viewOneFxmlLoader.load();
            addController(VIEW_ONE, viewOneFxmlLoader.getController());

            // View 2 fxml loader
            FXMLLoader viewTwoFxmlLoader = new FXMLLoader();
            viewTwoFxmlLoader.setLocation(TestApplication.class.getResource("views/two/two.fxml"));
            viewTwoFxmlLoader.setControllerFactory(injector::getInstance);
            viewTwo = viewTwoFxmlLoader.load();
            addController(VIEW_TWO, viewTwoFxmlLoader.getController());
        } catch (IOException e) {
            e.printStackTrace();
        }

        addView(VIEW_ONE, viewOne);
        addView(VIEW_TWO, viewTwo);

        setDefaultView(VIEW_ONE);
        setTitle("SFA FX Example Application");
    }

    @Override
    public void postInitialize() {
        addCommonCSS(TestApplication.class.getResource("css/styles.css").toExternalForm());
    }
}
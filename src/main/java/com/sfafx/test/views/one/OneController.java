package com.sfafx.test.views.one;

import com.sfafx.SingleFormApplication;
import com.sfafx.Switchable;
import com.sfafx.test.TestApplication;
import com.sfafx.test.dependencies.CommonData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class OneController implements Initializable, Switchable {
    @Inject
    CommonData commonData;

    @FXML
    VBox rootContainer;
    @FXML
    TextField textField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.textField.setText(commonData.getCommonString());
    }

    @FXML
    private void switchButtonAction() {
        this.commonData.setCommonString(this.textField.getText());
        SingleFormApplication.getInstance().switchView(TestApplication.VIEW_TWO);
    }

    @Override
    public void switchEvent() {
        this.textField.setText(commonData.getCommonString());
    }
}
